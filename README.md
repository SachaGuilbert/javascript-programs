Default syntax for basic javascript program i HTML:
```
<html>
<head>
<meta charset="utf-8"/>
<script language="javascript" type="text/javascript" src="p5.js"></script>
</head>
<body>
    <!--Write you HTML here-->
<script>
//Write you JavaScript here
var canvasWidth = 1200;
var canvasHeight = 800;
var setup = function() {
    createCanvas(canvasWidth, canvasHeight);
}

draw = function() {
    
};
</script>
</body>
</html>
```

In some cases such as programs with images, Firefox and google-chrome can't find image automatically. To fix this issue start a python server:
```
python3 -m http.server
```
